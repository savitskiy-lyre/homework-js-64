import React from 'react';
import './Contact.scss';

const Contact = () => {
   return (
     <div className="connect-form">
        <div className="container">
           <h3>connnect with us<span>Vivamus vestibulum nulla nec ante</span></h3>
           <div className="connect-form-block">
              <form action="#" method="post" onSubmit={(e) => e.preventDefault()}>
                 <input type="text" name="name" id="name" placeholder="Enter your name"/>
                 <input type="tel" name="phone" id="phone" placeholder="Enter your phone"/>
                 <textarea name="message" id="message" cols="30" rows="6"
                           placeholder="Enter your message"/>
                 <button type="submit">Submit</button>
              </form>
           </div>
        </div>
     </div>
   )
}

export default Contact;