import React, {useEffect, useState} from 'react';
import {useEditor, EditorContent} from '@tiptap/react';
import Preloader from "react-preloader-icon/Preloader";
import {Bars, Spinning} from "react-preloader-icon";
import {ADD_POST_URL, POSTS_URL} from "../../config";
import StarterKit from '@tiptap/starter-kit';
import Image from '@tiptap/extension-image';
import dayjs from 'dayjs';
import axios from "axios";
import MenuBar from "./MenuBar/MenuBar";
import './Tiptap.scss';
import './AddPost.css';

const AddPost = ({history, match}) => {
   let initRequesting = true;
   if (match.path.split('edit').join('') !== match.path) {
      initRequesting = false;
   }
   const [post, setPost] = useState(null);
   const [isNotRequesting, setIsNotRequesting] = useState(initRequesting);
   const [title, setTitle] = useState('');
   const [isAdded, setIsAdded] = useState(false);
   const editor = useEditor({
      extensions: [
         StarterKit,
         Image,
      ],
      content: post ? post.content : '',
   }, [post]);

   const addPost = async (e) => {
      e.preventDefault();
      setIsAdded(true);
      if (match.params.id) {
         const createdPostData = {
            title: title,
            content: editor.getHTML(),
            time:'redact ' +  dayjs().format('DD.MM.YYYY HH:mm:ss'),
         };
         await axios.put(POSTS_URL + '/' + match.params.id + '.json', createdPostData);
         history.push('/posts/' + match.params.id);
      } else {
         const createdPostData = {
            title: title,
            content: editor.getHTML(),
            time: dayjs().format('DD.MM.YYYY HH:mm:ss'),
         };
         await axios.post(POSTS_URL + '.json', createdPostData)
         history.push('/');
      }
   };

   useEffect(() => {
      if (!isNotRequesting) {
         const requestPost = async () => {
            const {data} = await axios.get(POSTS_URL + '/' + match.params.id + '.json')
            setPost(data);
            setTitle(data.title);
            setIsNotRequesting(true);
         }
         requestPost();
      }
   }, [isNotRequesting, match]);

   useEffect(() => {
      if (match.url + '/' === ADD_POST_URL && post) {
         setPost(null);
         setTitle('');
      }
   }, [post, match])

   return isNotRequesting ? (
     <form className="AddPost-wrapper" onSubmit={addPost}>
        {isAdded && (
          <div style={
             {
                position: 'absolute',
                left: '0',
                top: '0',
                display: 'flex',
                zIndex: '1000',
                background: 'rgba(239,238,238,0.7)',
                width: '100%',
                height: '100%',
             }}>
             <div style={{margin: 'auto'}}>
                <Preloader
                  use={Bars}
                  size={132}
                  strokeWidth={10}
                  strokeColor="black"
                  duration={1200}
                />
             </div>
          </div>)}
        <h3 className="post-title">{!initRequesting ? 'Redact post' : 'Add new post'}</h3>
        <div>
           <div>Title:</div>
           <input type="text" value={title} onChange={(e) => {
              setTitle(e.target.value);
           }} required/>
        </div>
        <div>
           <div>Description:</div>
           <div className="MenuBar">
              <MenuBar editor={editor}/>
              <EditorContent editor={editor}/>
           </div>
           <button className="addBtn">{!initRequesting ? 'Confirm ' : 'Add new post'}</button>
        </div>
     </form>
   ) : (
     <div style={{margin: '10% auto'}}>
        <Preloader
          use={Spinning}
          size={232}
          strokeWidth={10}
          strokeColor="#262626"
          duration={1200}
        />
     </div>
   );
};

export default AddPost;