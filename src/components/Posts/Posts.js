import React, {useEffect, useState} from 'react';
import {Spinning, Preloader} from "react-preloader-icon";
import axios from "axios";
import Post from "./Post/Post";
import './Posts.css';
import {POSTS_URL} from "../../config";

const Posts = () => {

   const [posts, setPosts] = useState(null);
   useEffect(() => {
      const requestPosts = async () => {
         try {
            const {data} = await axios.get(POSTS_URL + '.json');
            setPosts(data);
         } catch (e) {
            console.log(e);
         }
      }
      requestPosts();
   }, []);
   return (
     <div className="posts-block">
        <h3>My Blog</h3>
        <div className="posts-wrapper">
           {!posts ? (
             <div style={{margin: '50% 0'}}>
                <Preloader
                  use={Spinning}
                  size={232}
                  strokeWidth={10}
                  strokeColor="#262626"
                  duration={1200}
                />
             </div>
           ) : (
             Object.keys(posts).map((post) => {
                return <Post post={posts[post]} id={post} key={post}/>
             }).reverse()
           )}
        </div>
     </div>
   );
};
export default Posts;