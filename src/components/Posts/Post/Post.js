import React from 'react';
import {Link} from "react-router-dom";
import './Post.css';

const Post = ({post, id}) => {
   return (
     <div className='post-wrapper'>
        <div className="post-time">{post.time}</div>
        <div className="title">{post.title}</div>
        <Link to={'/posts/' + id}>Read more</Link>
     </div>
   );
};

export default Post;