import React from 'react';
import {NavLink} from 'react-router-dom';
import './NavBar.css';

const NavBar = () => {
   const activeNavItem = {
      color: 'orange',
      textDecoration: 'underline',
   }
   return (
     <ul className='NavBar'>
        <li>
           <NavLink exact to='/' activeStyle={activeNavItem}>Home</NavLink>
        </li>
        <li>
           <NavLink to='/posts/add' activeStyle={activeNavItem}>Add</NavLink>
        </li>
        <li>
           <NavLink to='/about' activeStyle={activeNavItem}>About</NavLink>
        </li>
        <li>
           <NavLink to='/contacts' activeStyle={activeNavItem}>Contacts</NavLink>
        </li>
     </ul>
   );
};

export default NavBar;