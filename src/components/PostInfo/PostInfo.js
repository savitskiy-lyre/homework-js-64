import React, {useEffect, useState} from 'react';
import {useEditor, EditorContent} from '@tiptap/react';
import {Oval, Preloader, Spinning} from "react-preloader-icon";
import Document from '@tiptap/extension-document';
import Paragraph from '@tiptap/extension-paragraph';
import Text from '@tiptap/extension-text';
import Image from '@tiptap/extension-image';
import {POSTS_URL} from "../../config";
import axios from "axios";
import './PostInfo.scss';

const PostInfo = ({routeObj}) => {
   const [postInfo, setPostInfo] = useState(null);
   const [onDelete, setOnDelete] = useState(false);
   const {match, history} = routeObj;
   const editor = useEditor({
      extensions: [
         Document,
         Paragraph,
         Text,
         Image,
      ],
      content: postInfo ? postInfo.content : '',
      editable: false,
   }, [postInfo])


   useEffect(() => {
      const requestPost = async () => {
         try {
            const {data} = await axios.get(POSTS_URL + '/' + routeObj.match.params.id + '.json');
            setPostInfo(data);
         } catch (e) {
            console.log(e);
         }
      }
      requestPost();
   }, [routeObj]);

   const onRedactPost = () => {
      history.push(match.url + '/edit');
   };
   const onDeletePost = async () => {
      setOnDelete(true);
      await axios.delete(POSTS_URL + '/' + match.params.id + '.json');
      history.replace('/');
   };


   return postInfo ? (
     <div className='post-info'>
        {onDelete && <div style={
           {
              position: 'absolute',
              left: '0',
              top: '0',
              display: 'flex',
              zIndex: '1000',
              background: 'rgba(222,222,222,0.7)',
              width: '100%',
              height: '100%',
           }}>
           <div style={{margin: 'auto'}}>
              <Preloader
                use={Oval}
                size={132}
                strokeWidth={10}
                strokeColor="lightblue"
                duration={1200}
              />
           </div>
        </div>}
        <div className="post-header">
           <div>
              <i>{postInfo.time}</i>
              <h4>{postInfo.title}</h4>
           </div>
           <div>
              <button onClick={onRedactPost}>Redact</button>
              <button onClick={onDeletePost}>Delete</button>
           </div>
        </div>
        <EditorContent editor={editor}/>
     </div>
   ) : (
     <div style={{margin: '10% auto'}}>
        <Preloader
          use={Spinning}
          size={232}
          strokeWidth={10}
          strokeColor="#262626"
          duration={1200}
        />
     </div>
   );
};
export default PostInfo;