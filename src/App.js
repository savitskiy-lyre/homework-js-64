import React from "react";
import axios from "axios";
import {Route, Switch} from "react-router-dom";
import {BASE_URL} from "./config";
import NavBar from "./components/NavBar/NavBar";
import Posts from "./components/Posts/Posts";
import PostInfo from "./components/PostInfo/PostInfo";
import AddPost from "./components/AddPost/AddPost";
import About from "./components/About/About";
import './App.css';
import Contact from "./components/Contact/Contact";

axios.defaults.baseURL = BASE_URL;

const App = () => {
   return (
     <div className="App">
        <NavBar/>
        <Switch>
           <Route exact path='/' component={() => <Posts/>}/>
           <Route exact path='/posts' component={() => <Posts/>}/>
           <Route path='/posts/add' component={AddPost}/>
           <Route path='/posts/:id/edit' component={AddPost}/>
           <Route path='/posts/:id' component={(routeObj) => <PostInfo routeObj={routeObj}/>}/>
           <Route path='/about' component={About}/>
           <Route path='/contacts' component={Contact}/>
        </Switch>
     </div>
   );
}

export default App;
