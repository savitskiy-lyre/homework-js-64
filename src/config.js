export const BASE_URL = 'https://tutorial-sample-posts-blog-default-rtdb.firebaseio.com/';
export const ADD_POST_URL = '/posts/add/';
export const POSTS_URL = '/posts';